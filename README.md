Clean Blog
================

This application requires:

- Ruby 2.3.1
- Rails 4.2.6

Learn more about [Installing Rails](http://railsapps.github.io/installing-rails.html).
