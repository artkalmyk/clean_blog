FactoryGirl.define do
  factory :post do
    user_id 1
    title "MyString"
    body "MyText"
    approved false
  end
end
