Rails.application.routes.draw do
  root to: 'posts#index'
  devise_for :users
  resources :users
  resources :posts
  get :user_posts, to: "posts#user_posts"
  resources :tags, only: [:index, :show]
  match "/posts/add_new_comment" => "posts#add_new_comment", as: "add_new_comment_to_posts", via: [:post]
  get :delete_comment, controller: 'posts', action: 'delete_comment'
 end
