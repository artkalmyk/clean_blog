var ready;

ready = function() {
  var counter, images;
  $('#slider > .sl').hide();
  $('#slider > .sl:nth-child(1)').show();
  counter = 0;
  images = $('#slider > .sl').length;
  $('#next').click(function(e) {
    var x;
    counter++;
    x = counter % images;
    if (x === 0) {
      $('#slider > .sl:nth-child(2)').hide();
      $('#slider > .sl:first-child').show();
      $('#slider > .sl:nth-child(3)').fadeOut();
    }
    if (x === 2 || x === -1) {
      $('#slider > .sl:nth-child(3)').fadeIn();
    }
    if (x === 1 || x === -2) {
      return $('#slider > .sl:nth-child(2)').fadeIn();
    }
  });
  return $('#prev').click(function(e) {
    var x;
    counter--;
    x = counter % images;
    if (x === 0) {
      $('#slider > .sl:nth-child(2)').fadeOut();
    }
    if (x === 1 || x === -2) {
      $('#slider > .sl:nth-child(2)').show();
      $('#slider > .sl:nth-child(3)').fadeOut();
    }
    if (x === 2 || x === -1) {
      return $('#slider > .sl:nth-child(3)').fadeIn();
    }
  });
};

$(function() {
  return ready();
});

$(document).on("page:update", function() {});

ready();
