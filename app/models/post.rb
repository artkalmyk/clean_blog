class Post < ActiveRecord::Base
  belongs_to :user
  acts_as_commentable
  acts_as_taggable_on :tags
  paginates_per 5
  scope :ordered, proc{ order(:created_at) }
  scope :approved, ->{where(approved: true)}
end
