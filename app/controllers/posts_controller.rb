class PostsController < ApplicationController
  before_action :set_post, only: [:edit, :update, :destroy]
  before_filter :authenticate_user!, only: [:create, :edit, :update, :destroy, :new, :user_posts]
  def index
    @posts = Post.ordered.approved.page params[:page]
    @all_tags = ActsAsTaggableOn::Tag.all
  end

  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end

  def edit
  end

  def create
    @post = current_user.posts.new(post_params)
    if @post.save
      redirect_to @post, notice: 'Post was successfully created.'
    else
      render :new
    end
  end

  def update
    if @post.update(post_params)
      redirect_to @post, notice: 'Post was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @post.destroy
    redirect_to posts_url, notice: 'Post was successfully destroyed.'
  end

  def add_new_comment
    post = Post.find(params[:id])
    post.comments << current_user.comments.create!(params.require(:comment).permit!)
    redirect_to action: :show, id: post, notice: 'Comment was successfully created.'
  end

  def delete_comment
    comment = current_user.comments.find(params[:comment_id])
    post = Post.find(params[:post_id])
    if comment.is_new?
      comment.destroy
      redirect_to action: :show, id: post, notice: 'Comment was successfully destroyed.'
    else
      redirect_to action: :show, id: post, notice: 'Comment was not destroyed.'
    end
  end

  def user_posts
    @posts = current_user.posts.ordered
  end

  private

    def set_post
      @post = current_user.posts.find(params[:id])
    end

    def post_params
      params.require(:post).permit(:user_id, :title, :body, :approved, :tag_list)
    end
end
